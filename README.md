Simple Deduplication Monitor (SDM)
===================================

##About
Simple Deduplication Monitor or SDM is a deduplication tool written in 
python which allows for configuring, customizing and analyzing deduplication.
Utilizing a multi-agent system approach, it is hopeful to decouple the processes 
to better analyze the mechanics of deduplication.

##Scope
The scope of the project is mainly for a school project assignment,
however it also ties into previous research done with 
[TRUST Lab](http://trust.dataengineering.org/research/).
 
##Requirements
- Python
- Datasets to deduplicate

##Usage
```
./sdm.py or python sdm.py
```

##Configuration
Edit the config.json in the root directory or use a custom file.
The configuration looks like this:
```
{
    "network": {
        "port": 8555,
        "backlog": 5
    },
    "directory": {
        "incoming": "./files",
        "temp": "./temp",
        "blocks": "./blocks",
        "manifests": "./manifests",
        "content-store": "."
    },
    "monitor": {
        "timeout": 1500
    },
    "filehandler": {
        "prefix": "fh-",
        "content-file": "fh-content.json"
    },
    "chunker": {
        "prefix": "chunker-",
        "spawn": 10,
        "chunksize": 32768
    },
    "deduplicator":{
        "prefix": "dedup-",
        "content-file": "dedup-content.json"
    }
}
```

Below is a table of common chunk sizes for the chunker in bytes.

| Representation | Bytes     |
| -------------- | ----------|
| 1KB            | `1024`    |
| 16KB           | `16384`   |
| 32KB           | `32768`   |
| 128KB          | `131072`  |
| 256KB          | `262144`  |
| 512KB          | `524288`  |
| 1MB            | `1048576` |

***

##Future Additions
 - Incorporate sliding window (variable width) chunk sizes and choose from static or variable widths
 - Store blocks in network/cloud storage
 - Generalize table information to be able to use an ORM like SQLAlchemy, SQLObject, Pony, etc.
 - Communicate with multiple SDM instances