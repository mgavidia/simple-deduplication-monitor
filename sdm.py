#!/usr/bin/env python

__author__ = 'Miguel A. Gavidia'
__license__ = 'BSD'
__version__ = '0.1'

import json
import os
import socket
import sys
#import threading
import multiprocessing
import datetime
import time
import select
import logging
from collections import deque
from sdm.core import agent
from sdm.core import utils
from sdm.agents.filehandler import FileHandler
from sdm.agents.chunker import Chunker
from sdm.agents.deduplicator import Deduplicator

CONFIG_FILE = os.path.abspath('config.json')
if len(sys.argv) == 2 and sys.argv[1] == '-h':
    print "usage: python {0} [-c configfile]".format(sys.argv[0])
    exit(0)
elif len(sys.argv) == 4 and sys.argv[1] == '-c':
    CONFIG_FILE = os.path.abspath(sys.argv[2])

def createagent(ClassName, config, name):
    ag = ClassName(config=config)
    ag.setName(name)
    ag.setsleep(config['settings']['sleep'])
    return ag

def filehandler(config, name):
    createagent(FileHandler, config, name).execute()

def chunker(config, name):
    createagent(Chunker, config, name).execute()

def deduplicator(config, name):
    createagent(Deduplicator, config, name).execute()

class AgentInfo:
    def __init__(self, config, target, name):
        self.name = name
        self.socket = None
        #self.thread = threading.Thread(target=target, args=(config, name,))
        self.thread = multiprocessing.Process(target=target, args=(config, name,))
        self.thread.start()
        self.ready = False

    def getname(self):
        return self.name

    def setsocket(self, sock):
        self.socket = sock

    def getsocket(self):
        return self.socket

    def send(self, message):
        utils.sendSocket(self.socket, message)

    def getthread(self):
        return self.thread

    def isready(self):
        return self.ready

    def setready(self, ready):
        self.ready = ready

class AgentLookup:
    def __init__(self, config):
        # Lookups
        self.filehandlers = []
        self.chunkers = []
        self.deduplicators = []
        self.currentfh = 0
        self.currentchunker = 0
        self.currentdeduplicator = 0
        self.config = config

    def getnextfilehandler(self):
        fh = self.filehandlers[self.currentfh]
        self.currentfh = (self.currentfh + 1) % len(self.filehandlers)
        return fh

    def getnextchunker(self):
        ch = self.chunkers[self.currentchunker]
        self.currentchunker = (self.currentchunker + 1) % len(self.chunkers)
        return ch

    def getnextdeduplicator(self):
        dedup = self.deduplicators[self.currentdeduplicator]
        self.currentdeduplicator = (self.currentdeduplicator + 1) % len(self.deduplicators)
        return dedup

    def addfilehandler(self):
        size = str(len(self.filehandlers) + 1)
        self.filehandlers.append(AgentInfo(self.config, filehandler, self.config['filehandler']['prefix'] + size))

    def addchunker(self):
        size = str(len(self.chunkers) + 1)
        self.chunkers.append(AgentInfo(self.config, chunker, self.config['chunker']['prefix'] + size))

    def adddeduplicator(self):
        size = str(len(self.deduplicators) + 1)
        self.deduplicators.append(AgentInfo(self.config, deduplicator, self.config['deduplicator']['prefix'] + size))

    def updatesocket(self, socket, message):
        def updater(handler, socket, message):
            for h in handler:
                if h.name == message['from']:
                    h.setsocket(socket)
                    h.setready(True)
                    return True
            return False
        if self.config['filehandler']['prefix'] in message['from']:
            if updater(self.filehandlers, socket, message):
                #print 'Added fh named: [{0}]'.format(message['from'])
                return
        elif self.config['chunker']['prefix'] in message['from']:
            if updater(self.chunkers, socket, message):
                #print 'Added fh named: [{0}]'.format(message['from'])
                return
        elif self.config['deduplicator']['prefix'] in message['from']:
            if updater(self.deduplicators, socket, message):
                #print 'Added fh named: [{0}]'.format(message['from'])
                return

    def allready(self):
        def check(handler):
            for h in handler:
                if not h.isready():
                    return False
            return True
        return check(self.filehandlers) and check(self.chunkers) and check(self.deduplicators)

    def termsockets(self, origin):
        def notify(handler):
            term = utils.Message("terminate")
            term['from'] = origin
            for h in handler:
                utils.sendSocket(h.getsocket(), term)
        notify(self.filehandlers)
        notify(self.chunkers)
        notify(self.deduplicators)

    def termthreads(self):
        def terminate(handler):
            for h in handler:
                h.getthread().join(1)
        terminate(self.filehandlers)
        terminate(self.chunkers)
        terminate(self.deduplicators)


def generateref():
    generateref.count += 1
    return generateref.count
generateref.count = 0


class TaskTrack:
    def __init__(self, message):
        self.reference = generateref()
        self.message = message
        self.message['ref'] = self.reference

    def __eq__(self, value):
        return self.reference == value

    def getref(self):
        return self.reference


class DedupMonitor(agent.AgentBase):
    def __init__(self, config):
        agent.AgentBase.__init__(self)
        self.setName('monitor')
        self.config = json.loads(open(config).read())

        # Timeout
        self.timeout = self.time() + self.config['monitor']['timeout']

        # Files Directory
        self.new_files = deque()
        self.book_keeping = []
        self.files_process = deque()
        self.chunkable = deque()
        self.chunks = deque()
        self.manifests = deque()

        # Setup service
        self.out('Starting service on port {0}'.format(self.config['network']['port']))
        self.server = utils.ServerSocket(self.config['network']['port'], self.config['network']['backlog'])

        # Agent Lookups
        self.agents = AgentLookup(self.config)

        # Tasks
        self.tasks = []

        logging.basicConfig(filename=self.config['settings']['logfile'], level=logging.DEBUG)

    def time(self):
        return int(round(time.time() * 1000))

    def removetask(self, ref):
        def index(num):
            for task in self.tasks:
                if task == num:
                    return self.tasks.index(task)
            return -1
        num = index(ref)
        if num > -1:
            self.out('Removing reference [{0}] from task pool'.format(ref))
            del self.tasks[num]

    def startagents(self):
        self.out('Initializing agents..')
        self.agents.addfilehandler()

        for x in range(0, self.config['chunker']['spawn']):
            self.agents.addchunker()

        self.agents.adddeduplicator()

    def pre(self):
        # Check directories
        dirs = ['blocks', 'manifests', 'temp']
        for name in dirs:
            directory = self.config['directory'][name]
            if not os.path.exists(directory):
                self.out('Creating {0} directory {1}'.format(name, directory))
                os.makedirs(directory)

        # Cleanup temp directory
        utils.deletedircontent(self.config['directory']['temp'])

        # Start agents
        self.startagents()

    def cleanup(self):
        self.out('Terminating.')
        self.out('Informing agents to shutdown..')
        self.agents.termsockets(self.getName())
        self.out('Killing rogue agents..')
        self.agents.termthreads()
        self.out('Killing service on port {0}'.format(self.config['network']['port']))
        self.server.close()
        self.out('Service stopped.')

    def directorycheck(self):
        for dirname, dirnames, filenames in os.walk(self.config['directory']['incoming']):
            for filename in filenames:
                f = os.path.join(dirname, filename)
                if f not in self.new_files and f not in self.book_keeping:
                    self.new_files.append(f)

    def percept(self):
        self.server.poll()
        self.directorycheck()

    def plan(self):
        if len(self.new_files):
            f = self.new_files.popleft()
            #self.out('Checking file [{0}] for processing'.format(f))
            if f not in self.book_keeping:
                self.book_keeping.append(f)
                self.files_process.append(f)

        if not self.server.isempty():
            client, message = self.server.next()
            if message == 'identify':
                self.out('Received ident from {0}'.format(message['from']))
                self.agents.updatesocket(client, message)
            elif message == 'verify-file-response':
                self.out('Received file response with reference [{0}]'.format(message['ref']))
                # Remove from task pool
                self.removetask(message['ref'])
                if message['have']:
                    self.out('No need to process file {0}'.format(message['filename']))
                else:
                    self.out('Initiating task of chunking file {0}'.format(message['filename']))
                    del message['from']
                    del message['to']
                    self.chunkable.append(message)
            elif message == 'cannot-chunk-file':
                # Readd to queue to send to next available chunker
                self.out('Chunker [{0}] unable to chunk file with reference [{1}], reassigning.'.format(message['from'],
                                                                                                        message['ref']))
                self.removetask(message['ref'])
                del message['from']
                del message['to']
                self.chunkable.append(message)
            elif message == 'chunk-ready':
                self.out('Received chunk for storage with reference [{0}]'.format(message['ref']))
                del message['from']
                del message['to']
                self.chunks.append(message)
            elif message == 'file-manifest':
                self.out('Received manifest for storage with reference [{0}]'.format(message['ref']))
                self.removetask(message['ref'])
                del message['from']
                del message['to']
                self.manifests.append(message)
            elif message == 'store-manifest-complete':
                self.out('File manifest storage with reference [{0}] completed'.format(message['ref']))
                self.removetask(message['ref'])
            elif message == 'verify-block-consumed':
                self.out('Block [{0}] successfully deduplicated by [{1}]'.format(message['hashid'], message['from']))
                self.removetask(message['ref'])
            else:
                self.out('Received unknown message: [{0}]'.format(message['command']))

    def action(self):
        #if self.time() <= self.timeout:
        #    return
        if not self.agents.allready():
            return

        # Processable files
        if len(self.files_process):
            path = self.files_process.popleft()
            filename = os.path.basename(path)
            md5 = utils.md5Checksum(path)
            self.out('Checking file {0} md5sum [{1}] if needs processing'.format(filename, md5))
            # Create task
            check = utils.Message('verify-file')
            check['from'] = self.getName()
            check['absolute-path'] = path
            check['filename'] = filename
            check['hashid'] = md5
            self.tasks.append(TaskTrack(check))
            # Get first one, never more than 1 (for now)
            fh = self.agents.getnextfilehandler()
            check['to'] = fh.getname()
            fh.send(check)

        # Chunkable files
        if len(self.chunkable):
            message = self.chunkable.popleft()
            self.out('Sending file [{0}] for chunking'.format(message['filename']))
            message.setCommand('chunk-file')
            message['from'] = self.getName()
            self.tasks.append(TaskTrack(message))
            ch = self.agents.getnextchunker()
            message['to'] = ch.getname()
            ch.send(message)

        # Received chunks
        if len(self.chunks):
            message = self.chunks.popleft()
            self.out('Sending block [{0}] for verification'.format(message['hashid']))
            message.setCommand('verify-block')
            message['from'] = self.getName()
            self.tasks.append(TaskTrack(message))
            de = self.agents.getnextdeduplicator()
            message['to'] = de.getname()
            de.send(message)

        # Received manifests
        if len(self.manifests):
            message = self.manifests.popleft()
            self.out('Sending manifest for [{0}] to storage'.format(message['filename']))
            message.setCommand('store-manifest')
            message['from'] = self.getName()
            self.tasks.append(TaskTrack(message))
            fh = self.agents.getnextfilehandler()
            message['to'] = fh.getname()
            fh.send(message)

    def execute(self):
        self.pre()
        while True:
            try:
                self.percept()
                self.plan()
                self.action()
                time.sleep(self.config['settings']['sleep'])
            except (KeyboardInterrupt, SystemExit):
                self.cleanup()
                raise

def now():
    return datetime.datetime.now().replace(microsecond=0)

starttime = now()

try:
    print 'Starting Simple Deduplication Monitor'
    sdm = DedupMonitor(CONFIG_FILE)
    print 'Startup took {0}'.format(now() - starttime)
    sdm.execute()
except socket.error as err:
    print 'Problem with socket. Error: {0}'.format(err)
    logging.critical('[{0}] - Problem with socket. Error is: {1}.'.format('SDM', err))
except (KeyboardInterrupt, SystemExit):
    pass
print 'Done! Total runtime: {0}'.format(now() - starttime)