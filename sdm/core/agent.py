# Agent base class

__author__ = 'Miguel A. Gavidia'
__license__ = 'BSD'
__version__ = '0.1'

import time

class AgentBase:
    def __init__(self):
        self.name = ''
        self.running = False
        self.sleep = 0.05

    def pre(self):
        pass

    def cleanup(self):
        pass

    def percept(self):
        pass

    def plan(self):
        pass

    def action(self):
        pass

    def execute(self):
        self.pre()
        self.toggleRunning()
        while self.running:
            try:
                self.percept()
                self.plan()
                self.action()
                time.sleep(self.sleep)
            except KeyboardInterrupt:
                pass
        self.cleanup()

    def getName(self):
        return self.name

    def setName(self, name):
        self.name = name

    def toggleRunning(self):
        self.running = not self.running

    def setsleep(self, sleep):
        self.sleep = sleep

    def out(self, text):
        print '[{0}] {1}'.format(self.getName(), text)
