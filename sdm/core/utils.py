
__author__ = 'Miguel A. Gavidia'
__license__ = 'BSD'
__version__ = '0.1'

import os
import shutil
import hashlib
import socket
import json
import base64
import select
import logging

from collections import deque


class Message:
    def __init__(self, text, isJson = False):
        self.values = {}
        if isJson:
            self.consumeJson(text)
        else:
            self.values["command"] = text
    
    def __getitem__(self, key):
        return self.values[key]
    
    def __setitem__(self, key, value):
        self.values[key] = value

    def __delitem__(self, key):
        try:
            del self.values[key]
        except KeyError:
            pass
        
    def __str__(self):
        return json.dumps(self.toJson(), sort_keys=True, indent=4, separators=(',', ': '))

    def __eq__(self, value):
        return self.values['command'] == value
        
    def toJson(self):
        commands = ','.join(self.values.keys())
        jsonMessage = {'___keylist___' : commands}
        for k in self.values.keys():
            jsonMessage[k] = self.values[k]
        return jsonMessage
        
    def consumeJson(self, string):
        # open('./json', 'w').write(string + "\n\n")
        jsonMessage = json.loads(string)
        for k in jsonMessage.keys():
            self.values[k] = jsonMessage[k]

    def setCommand(self, text):
        self.values['command'] = text

class FileNotFound(Exception):
    pass

class EndOfFile(Exception):
    pass

def md5Checksum(filePath):
    with open(filePath, 'rb') as fh:
        m = hashlib.md5()
        while True:
            data = fh.read(8192)
            if not data:
                break
            m.update(data)
        return m.hexdigest()

class File:
    def __init__(self, filename, size=1024):
        if not os.path.exists(filename):
            raise FileNotFound()
        self.filename = filename
        self.md5sum = md5Checksum(filename)
        self.file = open(filename, 'rb')
        self.size = size
    
    def next(self):
        chunk = self.file.read(self.size)
        size = len(chunk)
        if size == 0:
            raise EndOfFile()
        data = base64.b64encode(chunk)
        message = Message("file-send")
        message["size"] = size
        message["data"] = data
        message["hashid"] = hashlib.sha256(data).hexdigest()
        return message
        
    def getName(self):
        return os.path.basename(self.filename)
        
    def getSize(self):
        return os.path.getsize(self.filename)
        
    def getTotalChunks(self):
        total = self.getSize()
        return total / self.size + (1 if total % self.size else 0)
        
    def getMD5Sum(self):
        return self.md5sum
        
class ChunkFile:
    def __init__(self, tempdir, blocksize, filename, ref, md5sum=None):
        if not os.path.exists(filename):
            raise FileNotFound()
        self.blocksize = blocksize
        self.filename = filename
        self.refnum = ref
        self.md5sum = md5Checksum(filename) if not md5sum else md5sum
        self.blocks = []
        self.currentBlock = 0
        self.tempdir = tempdir + '/' + md5sum
        if not os.path.exists(self.tempdir):
            os.mkdir(self.tempdir)
        self.file = open(self.filename, 'rb')

    def next(self):
        block = self.file.read(self.blocksize)
        bytes_read = len(block)
        if bytes_read == 0:
            self.file.close()
            raise EndOfFile()
        encoded = base64.b64encode(block)
        datahash = hashlib.sha256(encoded).hexdigest()
        self.blocks.append([bytes_read, datahash])

        # Create block
        path = self.tempdir + '/{0}'.format(datahash)
        temp = open(path, 'wb')
        temp.write(block)
        temp.close()

        # Create message
        message = Message('chunk-ready')
        message['ref'] = self.refnum
        message['size'] = bytes_read
        message['hashid'] = datahash
        message['origin-filename'] = os.path.basename(self.filename)
        message['absolute-path'] = path

        return message

    def createManifest(self):
        manifest = {'filename': os.path.basename(self.filename), 'hashid': self.md5sum, 'blocks': []}
        count = 0
        for size, hashid in self.blocks:
            index = str(count)
            block = {index: {}}
            block[index]['size'] = size
            block[index]['hashid'] = hashid
            manifest['blocks'].append(block)
            count += 1
        return json.dumps(manifest)

    def getrefnum(self):
        return self.refnum

    def gethashid(self):
        return self.md5sum

    def getfilename(self):
        return os.path.basename(self.filename)
        
        
class FileSaver:
    def __init__(self, totalChunks, size, md5sum, filename):
        self.directory = '.'
        self.totalChunks = totalChunks
        self.size = size
        self.md5sum = md5sum
        self.filename = filename
        self.received = 0
        self.sizes = {}
    
    def next(self, chunk):
        # Save to disk first by sequence in case order is not correct
        temp = open(self.directory + "/" + str(chunk["sequence"]), 'wb')
        temp.write(base64.b64decode(chunk["data"]))
        self.received = self.received + 1
        self.sizes[chunk['sequence']] = chunk['size']
        temp.close()
        
    def hasAll(self):
        return self.totalChunks == self.received
        
    def writeFile(self):
        stitch = open(self.directory + "/" + self.filename, 'wb')
        for i in range(0, self.totalChunks):
            stitch.write(open(self.directory + "/" + str(i), 'rb').read(self.sizes[i]))
        stitch.close()
        
        returnable = False
        if self.md5sum == md5Checksum(self.directory + "/" + self.filename):
            returnable = True
        else:
            os.remove(self.directory + "/" + self.filename)
            returnable = False
            
        # Delete files
        for i in range(0, self.totalChunks):
            os.remove(self.directory + "/" + str(i))
            
        return returnable

def sendSocket(sock, message):
    #sock.setblocking(1)
    sendable = message.__str__()
    import struct
    size = struct.pack(">I", len(sendable))
    sock.sendall(size)
    sock.sendall(sendable)
    #sock.setblocking(0)

def readSocket(sock):
    import struct
    try:
        size = struct.unpack(">I", sock.recv(4))[0]
        data = sock.recv(size, socket.MSG_WAITALL)
        #print 'debug: {0} == {1} = {2}'.format(len(data), size, len(data) == size)
        return data
    except struct.error:
        #print 'Unable to read on socket. Closing.'
        logging.critical('Unable to read on socket {0}.'.format(socket))
        exit(0)

class ServerSocket:
    def __init__(self, port, backlog):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setblocking(0)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.bind(('localhost', port))
        self.server.listen(backlog)
        self.clients = [self.server]
        self.messages = deque()

    def poll(self):
        rready, wready, err = select.select(self.clients, [], [], 1)
        for s in rready:
            if s == self.server:
                # connection
                client, address = self.server.accept()
                self.clients.append(client)
            else:
                # Recieved a message
                data = readSocket(s)
                if data:
                    self.messages.append([s, Message(data, True)])
                else:
                    #self.out('Socket closed.')
                    logging.warning('Socket {0} closed.'.format(s))
                    s.close()
                    self.clients.remove(s)

    def getclients(self):
        #list = [element for element in self.clients if element != self.server]
        list = []
        for client in self.clients:
            if client == self.server:
                continue
            list.append(client)
        return list

    def send(self, sock, message):
        sendSocket(sock, message)

    def isempty(self):
        return not len(self.messages)

    def next(self):
        return self.messages.popleft()

    def close(self):
        self.server.close()

class ClientSocket:
    def __init__(self, host, port):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((host, port))
        self.sock.setblocking(1)
        self.messages = deque()

    def poll(self):
        ok = select.select([self.sock], [], [], 1)
        if ok[0]:
            self.messages.append(Message(readSocket(self.sock), True))

    def send(self, message):
        sendSocket(self.sock, message)

    def isempty(self):
        return not len(self.messages)

    def next(self):
        return self.messages.popleft()

    def close(self):
        self.sock.close()


class Content:
    def __init__(self, path):
        self.values = {}
        self.filename = path

    def __setitem__(self, key, value):
        self.values[key] = value

    def __getitem__(self, key):
        try:
            return self.values[key]
        except KeyError:
            return False

    def len(self):
        return len(self.values)

    def write(self):
        # Create File
        temp = open(self.filename, 'wb')
        temp.write(json.dumps(self.values))
        temp.close()

    def read(self):
        if not os.path.exists(self.filename):
            return
        # Read from file
        self.values = json.loads(open(self.filename).read())


def deletedircontent(folder):
    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception, e:
            logging.warning('Received error deleting content. Error is: {0}.'.format(e))
            print e


def movefile(src, dest):
    try:
        shutil.move(src, dest)
    except Exception, e:
        logging.warning('Could not move file [{0}] to [{1}]. Error is: {2}.'.format(src, dest, e))

def directoryempty(folder):
    for dirpath, dirnames, files in os.walk(folder):
        return not files

class FileWriter:
    def __init__(self, filename):
        try:
            os.remove(filename)
        except OSError:
            pass
        self.filename = filename

    def write(self, content):
        with open(self.filename, 'a') as output:
            output.write(content + '\n')