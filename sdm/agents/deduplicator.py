# Deduplicator agent

__author__ = 'Miguel A. Gavidia'
__license__ = 'BSD'
__version__ = '0.1'

import os
import json
import select
import socket
import sys
import logging
dir = os.path.dirname(os.path.abspath(__file__)) + '/../../'
sys.path.insert(0, dir)
from sdm.core import utils
from sdm.core import agent


class Deduplicator(agent.AgentBase):
    def __init__(self, configfile=None, config=None):
        agent.AgentBase.__init__(self)
        if configfile:
            config = json.loads(open(configfile).read())
        self.config = config
        self.blocks = utils.Content('{0}/{1}'.format(self.config['directory']['content-store'],
                                                        self.config['deduplicator']['content-file']))
        self.sock = utils.ClientSocket('localhost', config['network']['port'])

    def movefile(self, path):
        dir = os.path.dirname(path)
        filename = os.path.basename(path)
        dest = '{0}/{1}'.format(self.config['directory']['blocks'], filename)
        utils.movefile(path, dest)
        #if utils.directoryempty(dir):
        #    os.rmdir(dir)

    def pre(self):
        # Load content file
        self.out('Loading content table...')
        self.blocks.read()
        # Identify
        ident = utils.Message("identify")
        ident['from'] = self.getName()
        self.sock.send(ident)

    def percept(self):
        try:
            self.sock.poll()
        except socket.error as err:
            self.out('Problem reading on socket! \nError: {0}\nShutting down..'.format(err))
            logging.critical('[{0}] - Problem with socket. Error is: {1}.'.format(self.name, err))
            self.sock.close()
            self.toggleRunning()

    def action(self):
        while not self.sock.isempty():
            message = self.sock.next()
            if message == 'terminate':
                self.out('Terminate requested. Shutting down..')
                self.toggleRunning()
            elif message == 'verify-block':
                message['to'] = message['from']
                message['from'] = self.getName()
                if self.blocks[message['hashid']]:
                    self.out('Already have block [{0}], updating table'.format(message['hashid']))
                    blockcontent = self.blocks[message['hashid']]
                    blockcontent['count'] += 1
                    blockcontent['last-seen'] = self.blocks.len()
                    blockcontent['files'].append(message['origin-filename'])
                    self.blocks[message['hashid']] = blockcontent
                else:
                    self.out('Block [{0}] not found, adding to table'.format(message['hashid']))
                    blockcontent = {
                        'path': '{0}/{1}'.format(self.config['directory']['blocks'], message['hashid']),
                        'count': 1,
                        'last-seen': self.blocks.len() + 1,
                        'files': [message['origin-filename']],
                        'size': message['size']
                    }
                    self.movefile(message['absolute-path'])
                    self.blocks[message['hashid']] = blockcontent

                # Send verification back
                message['to'] = message['from']
                message['from'] = self.getName()
                message.setCommand('verify-block-consumed')
                self.sock.send(message)
            else:
                self.out('Received unknown message: [{0}]'.format(message['command']))

    def writeHRF(self):
        filename = '{0}/{1}'.format(self.config['directory']['content-store'],
                                    self.config['deduplicator']['content-file'])
        # Create hrf
        hrf = utils.FileWriter(filename.replace('.json', '.hrf'))
        hrf.write(' <count> [files] <hash> <size>')
        for hashid in self.blocks.values.iterkeys():
            hrf.write(' <{0}> [{1}] <{2}> <{3}>'.format(self.blocks[hashid]['count'],
                                                        ','.join(self.blocks[hashid]['files']),
                                                        hashid,
                                                        self.blocks[hashid]['size']))


    def cleanup(self):
        self.sock.close()
        # Write content table
        self.out('Writing content table..')
        self.blocks.write()
        self.out('Writing HRF..')
        self.writeHRF()