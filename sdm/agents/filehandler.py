
__author__ = 'Miguel A. Gavidia'
__license__ = 'BSD'
__version__ = '0.1'

import os
import json
import select
import socket
import sys
import logging

dir = os.path.dirname(os.path.abspath(__file__)) + '/../../'
sys.path.insert(0, dir)
from sdm.core import utils
from sdm.core import agent


class FileHandler(agent.AgentBase):
    def __init__(self, configfile=None, config=None):
        agent.AgentBase.__init__(self)
        if configfile:
            config = json.loads(open(configfile).read())
        self.config = config
        self.manifests = utils.Content('{0}/{1}'.format(self.config['directory']['content-store'],
                                                        self.config['filehandler']['content-file']))
        self.sock = utils.ClientSocket('localhost', config['network']['port'])

    def pre(self):
        # Load content file
        self.out('Loading content table...')
        self.manifests.read()
        # Identify
        ident = utils.Message("identify")
        ident['from'] = self.getName()
        self.sock.send(ident)

    def percept(self):
        try:
            self.sock.poll()
        except socket.error as err:
            self.out('Problem reading on socket! \nError: {0}\nShutting down..'.format(err))
            logging.critical('[{0}] - Problem with socket. Error is: {1}.'.format(self.name, err))
            self.sock.close()
            self.toggleRunning()

    def action(self):
        while not self.sock.isempty():
            message = self.sock.next()
            if message == 'terminate':
                self.out('Terminate requested. Shutting down..')
                self.toggleRunning()
            elif message == 'verify-file':
                # self.out(message)
                if self.manifests[message['hashid']]:
                    self.out('Already have the file [{0}]'.format(message['filename']))
                    message['to'] = message['from']
                    message['from'] = self.getName()
                    message['have'] = True
                    message.setCommand('verify-file-response')
                else:
                    self.out('Need file [{0}]'.format(message['filename']))
                    message['to'] = message['from']
                    message['from'] = self.getName()
                    message['have'] = False
                    message.setCommand('verify-file-response')
                self.sock.send(message)
            elif message == 'store-manifest':
                self.out('Storing file manifest for file [{0}]'.format(message['filename']))
                path = '{0}/{1}.manifest'.format(self.config['directory']['manifests'], message['hashid'])
                utils.movefile(message['absolute-path'], path)
                self.manifests[message['hashid']] = path
                message['to'] = message['from']
                message['from'] = self.getName()
                message.setCommand('store-manifest-complete')
                self.sock.send(message)
            else:
                self.out('Received unknown message: [{0}]'.format(message['command']))

    def cleanup(self):
        self.sock.close()
        # write content table
        self.out('Writing content table..')
        self.manifests.write()