# Chunker Agent

__author__ = 'Miguel A. Gavidia'
__license__ = 'BSD'
__version__ = '0.1'

import os
import json
import select
import socket
import sys
import logging
dir = os.path.dirname(os.path.abspath(__file__)) + '/../../'
sys.path.insert(0, dir)
from sdm.core import utils
from sdm.core import agent


class Chunker(agent.AgentBase):
    def __init__(self, configfile=None, config=None):
        agent.AgentBase.__init__(self)
        if configfile:
            config = json.loads(open(configfile).read())
        self.config = config
        self.sock = utils.ClientSocket('localhost', config['network']['port'])
        self.busy = False
        self.filetochunk = None

    def pre(self):
        # Identify
        ident = utils.Message("identify")
        ident['from'] = self.getName()
        self.sock.send(ident)

    def percept(self):
        try:
            self.sock.poll()
        except socket.error as err:
            self.out('Problem reading on socket! \nError: {0}\nShutting down..'.format(err))
            logging.critical('[{0}] - Problem with socket. Error is: {1}.'.format(self.name, err))
            self.sock.close()
            self.toggleRunning()

    def action(self):
        if not self.sock.isempty():
            message = self.sock.next()
            if message == 'terminate':
                self.out('Terminate requested. Shutting down..')
                self.toggleRunning()
            elif message == 'chunk-file':
                if not self.busy:
                    self.out('Got file [{0}] for chunking'.format(message['filename']))
                    self.filetochunk = utils.ChunkFile(self.config['directory']['temp'],
                                                       self.config['chunker']['chunksize'],
                                                       message['absolute-path'],
                                                       message['ref'],
                                                       message['hashid'])
                    self.busy = True
                else:
                    self.out('I\'m busy, cannot process file [{0}] sending back request.'.format(message['filename']))
                    message['to'] = message['from']
                    message['from'] = self.getName()
                    message.setCommand('cannot-chunk-file')
                    self.sock.send(message)
            else:
                self.out('Received unknown message: [{0}]'.format(message['command']))

        if self.filetochunk:
            try:
                message = self.filetochunk.next()
                message['from'] = self.getName()
                self.sock.send(message)
            except utils.EndOfFile:
                manifest = self.filetochunk.createManifest()
                #self.out(manifest)

                # Create manifest file
                path = self.config['directory']['temp'] + '/{0}.manifest'.format(self.filetochunk.gethashid())
                temp = open(path, 'wb')
                temp.write(manifest)
                temp.close()

                # Create message
                message = utils.Message('file-manifest')
                message['from'] = self.getName()
                message['absolute-path'] = path
                message['ref'] = self.filetochunk.getrefnum()
                message['filename'] = self.filetochunk.getfilename()
                message['hashid'] = self.filetochunk.gethashid()

                self.sock.send(message)

                self.filetochunk = None
                self.busy = False

    def cleanup(self):
        self.sock.close()